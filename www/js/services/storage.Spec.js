'use strict';

describe('local storage module', () => {
  let storage, $rootScope, $timeout;

  beforeEach(module('fast'));

  beforeEach(inject((_storage_, _$rootScope_, _$timeout_) => {
    storage = _storage_;
    $rootScope = _$rootScope_;
    $timeout = _$timeout_;
  }));

  describe('local storage module testing', () => {
    it('should be a storage has method put', () => {
      expect(storage.put).toBeDefined;
    });

    it('should be a method put save a json in storage', done => {
      const data = { token: 'heoaheoahoasdf123sdf' };
      storage.put('token', data)
        .then(item => {
          expect(item).toEqual(data);
          done();
        });
      $rootScope.$digest();
    });

    it('should be a method get toBeDefined', () => {
      expect(storage.get).toBeDefined;
    });

    it('should be a method get return one data', done => {
      const data  = { token: 'heoaheoahoasdf123sdf' };
      storage.get('token')
        .then(token => {
          expect(token).toEqual(data);
          done();
        });

      $rootScope.$digest();
    });
  });

  it('should be a method del toBeDefined', () => {
    expect(storage.del).toBeDefined;
  });


  it('should be a method del call with valid key', done => {
    storage.del('token');
    $timeout.flush()
    storage.get('token')
      .then(token => {
        expect(token).toBe(null);
        done();
      });
    $rootScope.$digest();
  });

  it('should be a method clear toBeDefined', () => {
    expect(storage.clear).toBeDefined();
  });

  it('should be clear will clear all localStorage', done => {
    storage.put('a', 'a');
    storage.clear();
    $timeout.flush();
    storage.get('a')
      .then(a => {
        expect(a).toBe(null);
        done();
      });
    $rootScope.$digest();
  });
});
