(function () {
  'use strict';

  angular
    .module('fast')
    .service('Ajax', ajax);

  ajax.$inject = ['$http'];

  function ajax ($http) {

    this.post = post;
    this.get = get;
    this.put = put;
    this.del = del;

    function post (url, body, config) {
      return $http.post(url, body, config || {});
    }

    function get (url) {
      return $http.get(url);
    }

    function put (url, body) {
      return $http.put(url, body);
    }

    function del (url) {
      return $http.delete(url);
    }
  }
}());
