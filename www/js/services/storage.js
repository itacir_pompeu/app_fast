(function () {
  'use strict';

  angular
    .module('fast')
    .service('storage', storage);

  storage.$inject = ['$window', '$q', '$timeout'];

  function storage ($window, $q, $timeout) {

    this.put = put;
    this.get = get;
    this.del = del;
    this.clear = clear;

    function put (key, data) {
      $window.localStorage.setItem(key, JSON.stringify(data));
      return this.get(key);
    }

    function get (key) {
      return $q.when(JSON.parse($window.localStorage.getItem(key)));
    }

    function del (key) {
      $timeout(() => {
        $window.localStorage.removeItem(key);
      });
    }

    function clear () {
      $timeout(() => {
        $window.localStorage.clear();
      });
    }
  }
}());
