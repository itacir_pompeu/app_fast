'use strict';

describe('ajax module', () => {
  let ajax, $httpBackend;
  const url = 'https://sample/api/user';

  beforeEach(module('fast'));

  beforeEach(inject((_Ajax_, _$httpBackend_) => {
    ajax = _Ajax_;
    $httpBackend = _$httpBackend_;
  }));

  describe('ajax module testing', () => {
    it('should be ajax has method post', () => {
      expect(ajax.post).toBeDefined();
    });

    it('should be method post create one resource', done => {
      const body = {
        name:  'Pompeu',
        email: 'ia@net.com'
      };

      const config = {
        authorization: 'Bearer rhoeajoljdslf'
      };

      $httpBackend
        .expect('POST', url)
        .respond(201, body);

      ajax.post(url, body, config).then(data => {
        expect(data.data).toEqual(body);
        expect(data.status).toBe(201);
        done();
      });
      $httpBackend.flush();
    });

    it('should be method post create one resource with out config', done => {
      const body = {
        name:  'Pompeu',
        email: 'ia@net.com'
      };

      $httpBackend
        .expect('POST', url)
        .respond(201, body);

      ajax.post(url, body).then(data => {
        expect(data.data).toEqual(body);
        expect(data.status).toBe(201);
        done();
      });
      $httpBackend.flush();
    });
  });

  it('should be ajax has method get', () => {
    expect(typeof ajax.get === 'undefined').toBe(false);
  });

  it('should be ajax has method get', done => {
    const user = {
      _id:   1,
      name: 'Pompeu'
    };

    $httpBackend
      .expect('GET', `${url}/${user._id}`)
      .respond(200, user);

    ajax.get(`${url}/${user._id}`).then(data => {
      expect(data.data).toEqual(user);
      expect(data.status).toBe(200);
      done();
    });
    $httpBackend.flush();
  });

  it('should be ajax has method put', () => {
    expect(typeof ajax.put === 'undefined').toBe(false);
  });

  it('should be ajax put update one resource', done => {
    const user = {
      _id:   1,
      name: 'Pompeu Limp'
    };

    $httpBackend
      .expect('PUT', `${url}/${user._id}`)
      .respond(202, user);

    ajax.put(`${url}/${user._id}`).then(data => {
      expect(data.data).toEqual(user);
      expect(data.status).toBe(202);
      done();
    });

    $httpBackend.flush();
  });

  it('should be ajax has method del', () => {
    expect(typeof ajax.del === 'undefined').toBe(false);
  });

  it('should be method del remove resource', done => {
    const user = {
      _id: 1
    };

    $httpBackend
      .expect('DELETE', `${url}/${user._id}`)
      .respond(204, user);

    ajax.del(`${url}/${user._id}`).then(data => {
      expect(data.status).toBe(204);
      done();
    });

    $httpBackend.flush();
  });
});
