(function () {
  'use strict';


  angular
    .module('fast') 
    .config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {
      $stateProvider
        .state('login', {
          url: '/login',
          templateUrl: './js/login.form.html',
          controller: 'LoginCtrl'
        });

      $urlRouterProvider.otherwise("/");
    }])
    .controller("LoginCtrl", ["$scope", "$state", function($scope, $state) {   
      $scope.user = {
        email: "",
        password : "",
      };

      $scope.login = function(form) {
        if(form.$valid) {
        }
      }; 
    }]);

}());
